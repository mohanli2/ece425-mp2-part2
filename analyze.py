#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 21:06:16 2019
analyze logging
@author: chen
"""
import matplotlib.pyplot as plt
import matplotlib
import statistics
from collections import defaultdict
import numpy as np
def get_log():
    #read server log:
    server_local_time = []
    server_abso_time = []
    server_tx_id = []
    send_time = dict()
    with open("server.log",'r') as file:
        for line in file:
            local_time,abso_time, tx_id,_ ,_,_ = line.split()
            server_local_time.append(local_time)
            server_abso_time.append(abso_time)
            server_tx_id.append(tx_id)
            send_time[tx_id] = abso_time
    #read clients' log
    files = [f"node{i}_transaction.log" for i in range(1,101)]
    
    num_nodes_reached = dict()
    time_used_to_reach = dict()
    #initialize as 0
    for tx in server_tx_id:
        num_nodes_reached[tx] = 0
        time_used_to_reach[tx] = []
    
    #read each file:
    for file_i in files:
        with open(file_i,'r') as file:
            for i,line in enumerate(file):
                _,abso_time, tx_id,_ ,_,_  = line.split()
                if tx_id in num_nodes_reached:
                    num_nodes_reached[tx_id] += 1
                    time_used_to_reach[tx_id].append(float(abso_time)-float(send_time[tx_id]))
    return server_abso_time,server_tx_id,num_nodes_reached,time_used_to_reach



def analyze_block():
    #read server log:
    server_local_time = []
    server_abso_time = []
    server_tx_id = []
    send_time = dict()
    with open("server.log",'r') as file:
        for line in file:
            local_time,abso_time, tx_id,_ ,_,_ = line.split()
            server_local_time.append(local_time)
            server_abso_time.append(abso_time)
            server_tx_id.append(tx_id)
            send_time[tx_id] = abso_time
            
            
    #read bolcks' info
    
    time_used_to_commit = dict()  # key: tx_id, value: reveived abso_time
    for tx in server_tx_id:
        time_used_to_commit[tx] = []
    
    comit_block_id = {}
    comit_block_time = []
    files = [f"node{i}_block.log" for i in range(1,101)]
    for i,file_i in enumerate(files):
        with open(file_i,'r') as file:
            data = file.read() 
            record_block_times = []#list for comit time of each block at each node
            blocks = data.split('TIME:')
            
            for block in blocks[1:]:
                record_t= block.split('\n')[0]
                block_solu = block.split('\n')[-3]
                #comit_block_id[block_solu] = 
                block_id = int(block.split('\n')[1].split()[1])
                comit_block_id[block_solu] = block_id
                logs = block.split('\n')[3:-3]
                for log in logs:
                    tx_id_client = log.split()[2]
                    time_used_to_commit[tx_id_client].append(float(record_t) - float(send_time[tx_id_client]))
                    
                record_block_times.append(record_t+ ' '+ block_solu)  
        comit_block_time.append(record_block_times)           
    return time_used_to_commit,comit_block_time,comit_block_id
                
time_used_to_commit,comit_block_time ,comit_block_id= analyze_block()
#How long does each transaction take to appear in a block? Are there congestion delays?
#time_used_to reach 
    
not_commtted = []
plt.figure(figsize =(8,6) )
for i,key in enumerate(time_used_to_commit):
    list_ = time_used_to_commit[key]
    if len(list_) == 0: #tx not committed
        not_commtted.append(key)
    else:
        plt.plot([i]*len(list_),list_,'bo',alpha = 0.2,markersize =3)
#plt.ylim((0,500))
#plt.text(100,300,f'{len(not_commtted)} transcations did not appear in any blocks')
plt.xlabel("Transaction number")
plt.ylabel('Time used for tx \n to appear in block (s)')
plt.title('Time used to commit for each transcation at each node \n (20 Node)')
plt.tight_layout()
plt.savefig('cp2_F3_100node_2.pdf')



#How long does a block propagate throughout the entire network?




block_dict = defaultdict(list)
for i,node_log in enumerate(comit_block_time):
    for block_time in node_log:
        time, solu = block_time.split()
        block_dict[solu].append(float(time)) 

propogated_blocks = {}
for k,v in block_dict.items():
    if len(v)!=1:
        propogated_blocks[k] = v

#plot
plt.figure(figsize=(8,6))
for i,key in enumerate(propogated_blocks):
    list_ = np.array(propogated_blocks[key])
    list__ = list_ - np.min(list_)
    plt.plot([i]*len(list__), list__, 'bo',alpha = 0.2,markeredgecolor = 'k')
plt.ylabel('Seconds')
plt.xlabel('Block number')
plt.title('Time used for a block \n to propogate to a node')
plt.savefig('cp2_F4_100node_2.pdf')



#how often do they split?
block_nums = [len(item) for item in comit_block_time]
largest_block_nums = max(block_nums)
#number of distint blocks for all nodes at each block number
sorted_b_id =  sorted( comit_block_id.items(), key = lambda kv: kv[1])

num_split = [ sum([value==i+1 for key,value in comit_block_id.items()])-1 for i in range(largest_block_nums) ]

plt.plot(figsize = (8,6))
plt.plot(range(largest_block_nums),num_split,'o-')
plt.xlabel('BLOCK ID')
plt.ylabel('Number of splits \n (distint blocks comitted)')
plt.tight_layout()
plt.savefig('split_100Node.pdf',dpi=200)



server_abso_time,server_tx_id,num_nodes_reached,time_used_to_reach = get_log()


font = {'size'   : 16}
matplotlib.rc('font', **font)

plt.figure(figsize = (8,6))
plt.plot(range(1,len(num_nodes_reached)+1),list(num_nodes_reached.values()),'.')
plt.ylabel('Number of nodes reached')
plt.xlabel("Transaction number")
plt.tight_layout()
plt.savefig('cp2_F1_100node.pdf',dpi=300)

#propagation delay
median_time_used_to_reach = [statistics.median(list_) for list_ in time_used_to_reach.values()]
max_time_used_to_reach = [max(list_) for list_ in time_used_to_reach.values()]
min_time_used_to_reach = [min(list_) for list_ in time_used_to_reach.values()]
plt.figure(figsize = (8,6))
plt.plot(range(1,len(num_nodes_reached)+1),median_time_used_to_reach,'r')
plt.plot(range(1,len(num_nodes_reached)+1),min_time_used_to_reach,'g')
plt.plot(range(1,len(num_nodes_reached)+1),max_time_used_to_reach,'b')
plt.legend(['Median','Minimum','Maximum'])
plt.xlabel('Transaction number')
plt.ylabel('Time used (seconds)')
plt.tight_layout()
plt.savefig('cp2_F2_100node.png',dpi=300)
          

plt.figure(figsize = (8,6))
plt.plot(range(1,len(num_nodes_reached)+1),median_time_used_to_reach,'r',alpha = 0.7)
plt.plot(range(1,len(num_nodes_reached)+1),min_time_used_to_reach,'g',alpha = 0.7)
plt.plot(range(1,len(num_nodes_reached)+1),max_time_used_to_reach,'b',alpha = 0.7)
plt.legend(['Median','Minimum','Maximum'])
plt.xlabel('Transaction number')
plt.ylabel('Time used (seconds)')
plt.ylim((0,1))
plt.tight_layout()
plt.savefig('cp2_F2_100node.png',dpi=300)