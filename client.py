#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 17:46:54 2019

@author: chen
"""

import sys
import socket
import threading
import time
import re
import logging
import random
import os
import hashlib
from collections import defaultdict


class Cryptocurrency(object):
    '''
	1. TCP
	2. Update nodes by sendind and receiving transactions
	3. Sending trÏansactions by gossip
    '''
    def __init__(self, node_name, node_port):
        self.node_IP = socket.gethostbyname(socket.gethostname())
        self.node_name = node_name
        self.node_port = node_port
        self.T = 4 # [s], time period for gossip node

        self.node = set() # known nodes, or neighbors
        self.received_txID = set() # all received tx id
        self.committed_txID = set() # all committed tx id, only committed a tx when creating a block
        self.node_log = self.create_logger(self.node_name + "_connection.log") # log connections between nodes
        self.block_log = self.create_logger(self.node_name + "_block.log") # log committted block
        self.transaction_log = self.create_logger(self.node_name + "_transaction.log") # log received tx
        self.split_log = self.create_logger(self.node_name + "_split.log") # log chain split

        self.blockID = 0 # increase blockID when: 1. received SOLVED from server, 2. received a block from other nodes
        self.block_solution = {} # solution of committed blocks, key is blockID, value is block solution
        self.block_solution[self.blockID] = self.compute_hash(str(random.random())) + '\n'
        self.tent_block = {}  # tentative block in dict, entrys separated by '\n'
        self.tent_block_batch = {} # tentative block batch in dict
        self.tent_batch_ths = 10 # recompute hash when batch size arrives 10
        self.unprocessed_block = [] # flag for processing received block
        #self.unprocessed_dict = {} # because block must be committed one by one, may need to sort unprocessed block by blockID
        self.received_block = set() # all received block, marked by block solution
        self.hash_block = {}   # hash - block dictionary. used when reveieve 'VERIFY OK' from server.

        self.balances = defaultdict(int) # initialize account, other accounts are all 0
        self.balances[0] = 1000000

    #### Thread 1 ####
    def listen_server(self):
        '''
        1. Join server
		2. Receive message: INTRODUCE, TRANSACTION and DIE from server
        '''
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((server_IP, server_port))
            s.send(f"CONNECT {self.node_name} {self.node_IP} {self.node_port}\n".encode())
            while True:
                data = s.recv(4096)
                if not data:
                    break               

                #####################################
                #### Process message from server ####
                #####################################
		        # INTRODUCE message: directly store the node, add node_log
                if data.decode().startswith('INTRODUCE'):
                    #if more than 1 introduce msg
                    data_ = data.decode().split('\n')
                    for data_i in data_[:-1]:
                        #print(data_i)
                        self.add_node(data_i)
				
                # TRANSACTION message
                #elif re.match(r"TRANSACTION\s+(.*)$", data.decode(), re.I):
                elif data.decode().startswith("TRANSACTION"):
                    # gossip transaction
                    data_ = data.decode().split('\n')
                    for data_i in data_[:-1]:
                        self.gossip_tx(data_i)

                # SOLVED message
                elif data.decode().startswith("SOLVED"):
                    # May already send a new SOLVE, may receive block from other nodes. Check puzzle. 
                    puzzle = ""
                    puzzle += self.block_solution[len(self.block_solution)-1] # get the last solution
                    puzzle += self.sequence_tent_block()
                    puzzle_hash = self.compute_hash(puzzle)
                    if puzzle_hash == data.decode().split()[1]:
                        print(f"BLOCK {self.blockID+1} get solved!")
                        self.split_log.info(f"BLOCK {self.blockID+1} get solved!")
                        # commit the block
                        self.commit_solved_block(data.decode())
                        # SOLVE the next puzzle
                        tent_block_hash = self.recompute_batch_hash()
                        s.send(("SOLVE " + tent_block_hash + '\n').encode())
                
                # VERIFY message
                elif data.decode().startswith("VERIFY"):
                    
                    try:
                        _ , ok, puzzle_hash, solu = data.decode().split()
                    except:
                        print(data.decode().split())
                    if ok == 'OK':
                        block = self.hash_block[puzzle_hash]
                        
                        # check duplicated tx in received block and self.tent + self.batch
                        # commite block
                        self.commit_verified_block(block)
                        # SOLVE the next puzzle
                        tent_block_hash = self.recompute_batch_hash()
                        s.send(("SOLVE " + tent_block_hash + '\n').encode())
                    else:
                        print('verify failed!!!!!!!!')
				
                # DIE message: 
                elif re.match(r"DIE\n$", data.decode(), re.I):
					# kill process
                    print("DIE...")
                    os._exit(0)
                
                else:
                    print("Unexpected msg from server")
                    print(data.decode())

                ######################################################
                #### Judge the object status for SOLVE and VERIFY ####
                ######################################################
                # check SOLVE
                if len(self.tent_block_batch) >= self.tent_batch_ths and len(self.tent_block)<=2000:
                    tent_block_hash = self.recompute_batch_hash()
                    s.send(("SOLVE " + tent_block_hash + '\n').encode())
                
                # check VERIFY 
                #if len(self.unprocessed_dict) >0:
                    #self.unprocessed_block.sort(reverse = True)
                    #sorted_unprocessed = sorted(self.unprocessed_dict.items(), key=lambda kv: kv[1]) # [(block, int),(),()]
                    #rb = sorted_unprocessed[0][0]
                    #self.unprocessed_dict.pop(rb,None)
                if len(self.unprocessed_block) > 0:
                    rb = self.unprocessed_block.pop()
                    solution = rb.split('\n')[-2] 
                    received_blockID = int(rb.split('\n')[0].split()[1])
                    if received_blockID > self.blockID: # only process block with larger length      
                        if received_blockID - self.blockID > 1: # buffer it
                            self.unprocessed_block = [rb] + self.unprocessed_block # put it to the begining of list
                            #self.unprocessed_dict[rb] = received_blockID
                            #self.unprocessed_block.sort(reverse = True)
                        else: # VERIFY
                            puzzle = rb.split('\n')[1:-2]
                            puzzle = "\n".join(puzzle) + '\n'
                            puzzle_hash = self.compute_hash(puzzle)
                            self.hash_block[puzzle_hash] = rb
                            s.send((f"VERIFY {puzzle_hash} {solution}\n").encode())


    #### Thread 2 ####
    def listen_node(self):
        '''
		listen to other nodes
		receive message: TRANSACTION, INTRODUCE
        '''
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as ss:
            ss.bind((self.node_IP, self.node_port))
            ss.listen(200)
            while True:
                conn, addr = ss.accept() # conn is a socket object
				# No way to know the receive port of other nodes, only know the send port of other nodes
                with conn:
                    while True:
                        data = conn.recv(800000)
                        if not data:
                            break
                        _data = data.decode()
                        while _data[-1] != '$':
                            _data = _data + conn.recv(80000).decode()
					   ### Process data ###
                        data = _data[:-1].encode()
                        # INTRODUCE from other nodes
                        if re.match(r"INTRODUCE\s+(.*)$", data.decode(), re.I):
                            self.add_node(data.decode())
                        
                        # TRANSACTION from other nodes
                        elif re.match(r"TRANSACTION\s+(.*)$", data.decode(), re.I):
							# check whether tx has been received 
                            tx_id = data.decode().split()[2]
                            if (tx_id not in self.received_txID) and (tx_id not in self.committed_txID):
                                self.gossip_tx(data.decode())
                        
                        # BLOCK from other nodes
                        elif data.decode().startswith("BLOCK"):
                            # check whether block has been received
                            block_solu = data.decode().split('\n')[-2]+'\n'
                            if block_solu not in self.received_block:
                                #do this after verified
                                self.received_block.add(block_solu)
                                self.unprocessed_block.append(data.decode())
                                #self.unprocessed_dict[data.decode()] = int(data.decode().split('\n')[0].split()[1])
                        
                        else:
                            print("Unexpected msg from other nodes")
                        

    #### Thread 3 ####
    def gossip_node(self):
        '''
        periodically gossip self to other nodes, period 4s
        data example: INTRODUCE 172.22.94.228 4444\n
        '''
        while True:
            #1. select self, select 2 konwn nodes randomly
            nd_set = set()
            nd_set.add(f"INTRODUCE node {self.node_IP} {self.node_port}")
            known_nd = set(random.sample(self.node, min(4, len(self.node)))) # OR: must send to 3 nodes successfully
            for nd in known_nd:
                nd_set.add(f"INTRODUCE node {nd[0]} {nd[1]}")
            #2. select 3 known nodes randomly
            gossip_node = set(random.sample(self.node, min(10, len(self.node))))
            for i in gossip_node:
                for nd in nd_set:
                    self.send_node(i, nd)
            time.sleep(self.T)
    

    #############################
    #### Auxiliary functions ####
    #############################
    def compute_hash(self, block):
        '''
        cpmpute hash for: 
        1. self.tent_block_str and send it to server for solving
        2. block received from other nodes and send it to server for verifying
        '''
        hash_object = hashlib.sha256(str.encode(block))
        hex_dig = hash_object.hexdigest() #str
        return hex_dig


    def create_logger(self, logname):
        # create logger
        logger = logging.getLogger(logname)
        logger.setLevel(logging.DEBUG)
        # creater file handler
        fh = logging.FileHandler(logname, mode='w')
        fh.setLevel(logging.DEBUG)
        # set file handler format
        formatter = logging.Formatter('%(message)s')
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        return logger


    def add_node(self, data):
        '''
		process INTRODUCE msg by add node into self.node
        '''
        #print(data)
        node_line = re.search(r"INTRODUCE\s+(.*)$", data, re.I).group(1)
		# node_line example: node1 172.22.156.2 4444
        new_node = (str(node_line.split()[1]), int(node_line.split()[2]))
        if new_node not in self.node:
            self.node.add(new_node)
            self.node_log.info(str(new_node) + " connected")
            #print("find node: " + str(new_node))


    def gossip_tx(self, data):
        '''
        data example: TRANSACTION 1553455263.260154 d94ca30d703c289582b6229533c03694 32914 736574 14\n
        '''
        # 1. record the time when receive message
        
        tx_time = time.time() 
        tx_id = data.split()[2]
        tx_from = data.split()[3]
        tx_to = data.split()[4]
        tx_amount = data.split()[5]
        tx = f"TRANSACTION {tx_time:#.6f} {tx_id} {tx_from} {tx_to} {tx_amount}"
        self.transaction_log.info(tx)
        tx += '\n'
        # 2. record ID
        self.received_txID.add(tx_id)
        self.tent_block_batch[tx_id] = tx
        # 3. gossip msg: randomly choose up to three nodes and send msg
        gossip_node = set(random.sample(self.node, min(3, len(self.node)))) # OR: must send to 3 nodes successfully
        for i in gossip_node:
            self.send_node(i, tx)

			
    def send_node(self, node, msg):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            ##### s.bind((self.node_IP, self.node_port)) #####
            # can not bind the msg receive port as the msg send port, have to gossip INTRODUCE msg
            s.connect(node)
            a = s.sendall((msg+'$').encode())
            if a!=None:
                print('!!!!!!!!')
                print(a)
                
        except:
            if node in self.node:
                self.node.remove(node)
                self.node_log.info(str(node) + " disconnected")
            #print("remove node: " + str(node))
        s.close()


    def commit_solved_block(self, data):
        '''
        process solved block from server
        data example: SOLVED 86777674e3fe09e0da911be4c7bce219794a8988955508d3e9433d8584630b1f 251a76a3bc860cb9f599bb2d6e183753120dcd26c211e3c20f16337b53e43bdb\n
            the first hash is the puzzle, the second hash is the solution computed by server
        '''
        # 1. update blockID
        solution = data.split()[2]+'\n'

        self.blockID += 1
        self.block_solution[self.blockID] = solution
        self.received_block.add(solution)
        # 2. gossip block
        block_str = "BLOCK " + str(self.blockID) + '\n'
        block_str += self.block_solution[(len(self.block_solution))-2] # solution of previous block
        block_str += self.sequence_tent_block()
        block_str += self.block_solution[(len(self.block_solution))-1] # solution of current block
        # 3. execute txs in block
        self.execute_block(block_str)
        # 4. clear tentative block for solving the new puzzle
        self.tent_block.clear()
        self.gossip_block(block_str)

    def commit_verified_block(self, data):
        '''
        data example: BLOCK ID\n, solution of the previous block\n, lots of transactions, solution of current block\n
        '''
        blockid, logs, solu_curr = data.split('\n')[0], data.split('\n')[2:-2], data.split('\n')[-2]
        #block id + 1
        _, block_id = blockid.split()
        self.blockID += 1
        #check if consistent:
        if int(block_id) != self.blockID:
            print('Inconsistent blockID, check')
            return
        #update
        self.block_solution[self.blockID] = solu_curr +'\n'
        self.received_block.add(solu_curr + '\n')
        # execite txs in block
        self.execute_block(data)
        #clear relevant tx data in tentative block
        for log in logs:
            tx_id = log.split()[2]
            self.tent_block_batch.pop(tx_id,None)
            self.tent_block.pop(tx_id,None)
        #gossip block
        self.gossip_block(data)
        

    def execute_block(self, data):
        '''
        data example: BLOCK ID\n, solution of the previous block\n, lots of transactions, solution of current block\n
        same as gossip_block()
        '''
        # 1. log block
        self.block_log.info('TIME:'+str(time.time()))
        self.block_log.info(data)
        # 2. split data
        txs = data.split('\n')[2:-2]
        print(f"Execute block {self.blockID}, length: " + str(len(txs)))
        for tx in txs:
            #print(tx)
            tx_id = tx.split()[2]
            tx_from = int(tx.split()[3])
            tx_to = int(tx.split()[4])
            tx_amount = int(tx.split()[5])
            # check whether tx has been committed
            if tx_id in self.committed_txID:
                continue
            # execute tx
            self.committed_txID.add(tx_id)
            if tx_id in self.received_txID:
                self.received_txID.remove(tx_id)
            if self.balances[tx_from] >= tx_amount: # accept transaction
                self.balances[tx_from] -= tx_amount
                self.balances[tx_to] += tx_amount
    
    
    def gossip_block(self, data):
        '''
        data example: BLOCK ID\n, solution of the previous block\n, lots of transactions, solution of current block\n
        '''
        gossip_node = set(random.sample(self.node, min(10, len(self.node)))) # OR: must send to 3 nodes successfully
        for i in gossip_node:
            self.send_node(i, data)


    def recompute_batch_hash(self):
        # 1. move txs in batch into tentative block
        for key in self.tent_block_batch:
            self.tent_block[key] = self.tent_block_batch[key]
        # 2. clear batch
        self.tent_block_batch.clear()
        # 3. sequence tentative block
        tent_block_str = ""
        tent_block_str += self.block_solution[len(self.block_solution)-1]
        tent_block_str += self.sequence_tent_block()
        # 4. compute hash of tentative block
        tent_block_hash = self.compute_hash(tent_block_str)
        return tent_block_hash


    def sequence_tent_block(self):
        tent_block_str = ""
        for key in self.tent_block:
            tent_block_str += self.tent_block[key]
        return tent_block_str


# main function
if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python36 client.py <node_name> <node_port>")
        sys.exit(1)
    
    server_IP = "172.22.94.228" # run server on vm1, i.e. the first vm
    server_port = 8888
    
    node_name = str(sys.argv[1]) # node1, node2...
    node_port = int(sys.argv[2]) # can not be 8888
    cc = Cryptocurrency(node_name, node_port)
    
    t1 = threading.Thread(target=cc.listen_server)
    t2 = threading.Thread(target=cc.listen_node)
    t3 = threading.Thread(target=cc.gossip_node)
    
    t1.start()
    t2.start()
    t3.start()

'''
summary:
1. only add node in add_nd()
2. only remove node when can be send msg to it
	1). INTRODUCE msg in gossip_node()
	2). TRANSACTION msg in send_node
'''